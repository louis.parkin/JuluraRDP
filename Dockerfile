FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /builder

COPY install_requirements_deb.sh .

RUN apt-get update && apt-get upgrade -y \
    && ./install_requirements_deb.sh
