#!/bin/bash

IMAGE=$1
if [ -z ${IMAGE} ]; then
	echo "Image base name missing"
	exit 1
fi

for SCALE in $(cat scales.txt); do
	if [ ! -d  ${SCALE} ]; then
		mkdir ${SCALE}
	fi

	inkscape -z -e "${SCALE}/${IMAGE}.png" -w ${SCALE} "${IMAGE}.svg"
	cwebp -q 75 "${SCALE}/${IMAGE}.png" -o "${SCALE}/${IMAGE}.webp"
done
