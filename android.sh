#!/bin/bash

# Clean workspace:
cmake clean . ; for FILE in $(find . | grep CMakeCache); do rm $FILE; done

# Setup environment
export NDK=/home/louis/Android/Sdk/ndk/21.3.6528147
export ANDROID_NDK=$NDK
export ANDROID_SDK=/home/louis/Android/Sdk
export PATH=$ANDROID_SDK/tools:${ANDROID_SDK}/platform-tools:$PATH
export PATH=$ANDROID_NDK/toolchains/llvm/prebuilt/linux-x86_64/bin/:$PATH
export PATH=$ANDROID_NDK/toolchains/llvm/prebuilt/linux-x86_64/bin/clang:$PATH


# Build modules for android JNI - Release build without H264 - uncomment to use
./scripts/android-build-freerdp.sh --conf ./scripts/android-build-release.conf --ndk $ANDROID_NDK --sdk $ANDROID_SDK

# Build modules for android JNI - Debug build without H264 - uncomment to use
# ./scripts/android-build-freerdp.sh --ndk ${ANDROID_NDK} --sdk ${ANDROID_SDK}
