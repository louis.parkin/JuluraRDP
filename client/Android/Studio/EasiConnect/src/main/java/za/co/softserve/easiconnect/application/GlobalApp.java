package za.co.softserve.easiconnect.application;

import com.google.firebase.crashlytics.FirebaseCrashlytics;

public class GlobalApp extends com.freerdp.freerdpcore.application.GlobalApp
{
    @Override
    public void onCreate() {
        super.onCreate();

        if(FirebaseCrashlytics.getInstance().didCrashOnPreviousExecution()) {
            FirebaseCrashlytics.getInstance().sendUnsentReports();
        }
    }
}
