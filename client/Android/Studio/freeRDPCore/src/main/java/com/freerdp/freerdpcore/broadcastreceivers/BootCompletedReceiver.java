package com.freerdp.freerdpcore.broadcastreceivers;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.freerdp.freerdpcore.presentation.HomeActivity;

public class BootCompletedReceiver extends BroadcastReceiver {
    private static final String TAG = "BOOTING";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (action != null) {
            Toast.makeText(context, action, Toast.LENGTH_LONG).show();
            if (action.equalsIgnoreCase("android.intent.action.BOOT_COMPLETED") ||
                    action.equalsIgnoreCase("android.intent.action.QUICKBOOT_POWERON")) {
                Toast.makeText(context, "Booted", Toast.LENGTH_LONG).show();

                Intent i = new Intent(context, HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    context.startActivity(i);
                } catch (ActivityNotFoundException e) {
                    String message = "za.co.softserve.easiconnect" + " not installed.";

                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                } catch (Exception ex) {
                    String message = ex.getMessage();

                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    ex.printStackTrace();
                }
            }
        }
    }
}
