package com.freerdp.freerdpcore.presentation;

import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.nfc.FormatException;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.text.TextUtilsCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.freerdp.freerdpcore.R;
import com.freerdp.freerdpcore.services.LibFreeRDP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Formatter;
import java.util.IllegalFormatException;
import java.util.Locale;
import java.util.Objects;

public class AboutActivity extends AppCompatActivity
{

	// To keep track of activity's window focus
	boolean currentFocus;

	// To keep track of activity's foreground/background status
	boolean isPaused;

	Handler collapseNotificationHandler;

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {

		currentFocus = hasFocus;

		if (!hasFocus) {

			// Method that handles loss of window focus
			collapseNow();
		}
	}

	public void collapseNow() {

		// Initialize 'collapseNotificationHandler'
		if (collapseNotificationHandler == null) {
			collapseNotificationHandler = new Handler();
		}

		// If window focus has been lost && activity is not in a paused state
		// Its a valid check because showing of notification panel
		// steals the focus from current activity's window, but does not
		// 'pause' the activity
		if (!currentFocus && !isPaused) {

			// Post a Runnable with some delay - currently set to 300 ms
			collapseNotificationHandler.postDelayed(new Runnable() {

				@Override
				public void run() {

					// Use reflection to trigger a method from 'StatusBarManager'

					Object statusBarService = getSystemService("statusbar");
					Class<?> statusBarManager = null;

					try {
						statusBarManager = Class.forName("android.app.StatusBarManager");
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}

					Method collapseStatusBar = null;

					try {

						collapseStatusBar = Objects.requireNonNull(statusBarManager).getMethod("collapsePanels");
						collapseStatusBar.setAccessible(true);
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					}



					try {
						collapseStatusBar.invoke(statusBarService);
					} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
						e.printStackTrace();
					}

					// Check if the window focus has been returned
					// If it hasn't been returned, post this Runnable again
					// Currently, the delay is 100 ms. You can change this
					// value to suit your needs.
					if (!currentFocus && !isPaused) {
						collapseNotificationHandler.postDelayed(this, 100L);
					}

				}
			}, 100L);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		// Activity's been paused
		isPaused = true;
	}

	private static final String TAG = AboutActivity.class.toString();
	private WebView mWebView;

	@Override protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		mWebView = (WebView)findViewById(R.id.activity_about_webview);
	}

	@Override protected void onResume()
	{
		populate();
		super.onResume();
		isPaused = false;
	}

	private void populate()
	{
		StringBuilder total = new StringBuilder();

		String filename = "about_phone.html";
		if ((getResources().getConfiguration().screenLayout &
		     Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE)
		{
			filename = "about.html";
		}
		Locale def = Locale.getDefault();
		String prefix = def.getLanguage().toLowerCase(def);

		String dir = prefix + "_about_page/";
		String file = dir + filename;
		InputStream is;
		try
		{
			is = getAssets().open(file);
			is.close();
		}
		catch (IOException e)
		{
			Log.e(TAG, "Missing localized asset " + file, e);
			dir = "about_page/";
			file = dir + filename;
		}

		try
		{
			try (BufferedReader r =
			         new BufferedReader(new InputStreamReader(getAssets().open(file))))
			{
				String line;
				while ((line = r.readLine()) != null)
				{
					total.append(line);
					total.append("\n");
				}
			}
			finally
			{
				r.close();
			}
		}
		catch (IOException e)
		{
			Log.e(TAG, "Could not read about page " + file, e);
		}

		// append FreeRDP core version to app version
		// get app version
		String version;
		try
		{
			version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		}
		catch (PackageManager.NameNotFoundException e)
		{
			version = "unknown";
		}
		version = version + " (" + LibFreeRDP.getVersion() + ")";

		WebSettings settings = mWebView.getSettings();
		settings.setDomStorageEnabled(true);
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setSupportZoom(true);

		final String base = "file:///android_asset/" + dir;

		final String rawHtml = total.toString();
		final String html = rawHtml.replaceAll("%AFREERDP_VERSION%", version)
		                        .replaceAll("%SYSTEM_VERSION%", Build.VERSION.RELEASE)
		                        .replaceAll("%DEVICE_MODEL%", Build.MODEL);

		mWebView.loadDataWithBaseURL(base, html, "text/html", null, "about:blank");
	}
}
