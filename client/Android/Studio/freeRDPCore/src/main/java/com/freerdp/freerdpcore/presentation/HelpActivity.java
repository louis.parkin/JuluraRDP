/*
   Activity that displays the help pages

   Copyright 2013 Thincast Technologies GmbH, Author: Martin Fleisz

   This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
   If a copy of the MPL was not distributed with this file, You can obtain one at
   http://mozilla.org/MPL/2.0/.
*/

package com.freerdp.freerdpcore.presentation;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;
import java.util.Objects;

public class HelpActivity extends AppCompatActivity
{

	private static final String TAG = HelpActivity.class.toString();

	// To keep track of activity's window focus
	boolean currentFocus;

	// To keep track of activity's foreground/background status
	boolean isPaused;

	Handler collapseNotificationHandler;

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {

		currentFocus = hasFocus;

		if (!hasFocus) {

			// Method that handles loss of window focus
			collapseNow();
		}
	}

	public void collapseNow() {

		// Initialize 'collapseNotificationHandler'
		if (collapseNotificationHandler == null) {
			collapseNotificationHandler = new Handler();
		}

		// If window focus has been lost && activity is not in a paused state
		// Its a valid check because showing of notification panel
		// steals the focus from current activity's window, but does not
		// 'pause' the activity
		if (!currentFocus && !isPaused) {

			// Post a Runnable with some delay - currently set to 300 ms
			collapseNotificationHandler.postDelayed(new Runnable() {

				@Override
				public void run() {

					// Use reflection to trigger a method from 'StatusBarManager'

					Object statusBarService = getSystemService("statusbar");
					Class<?> statusBarManager = null;

					try {
						statusBarManager = Class.forName("android.app.StatusBarManager");
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}

					Method collapseStatusBar = null;

					try {

						collapseStatusBar = Objects.requireNonNull(statusBarManager).getMethod("collapsePanels");
						collapseStatusBar.setAccessible(true);
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					}



					try {
						collapseStatusBar.invoke(statusBarService);
					} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
						e.printStackTrace();
					}

					// Check if the window focus has been returned
					// If it hasn't been returned, post this Runnable again
					// Currently, the delay is 100 ms. You can change this
					// value to suit your needs.
					if (!currentFocus && !isPaused) {
						collapseNotificationHandler.postDelayed(this, 100L);
					}

				}
			}, 100L);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		// Activity's been paused
		isPaused = true;
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Activity's been resumed
		isPaused = false;
	}

	@Override public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		WebView webview = new WebView(this);
		setContentView(webview);

		String filename;
		if ((getResources().getConfiguration().screenLayout &
		     Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE)
			filename = "gestures.html";
		else
			filename = "gestures_phone.html";

		WebSettings settings = webview.getSettings();
		settings.setDomStorageEnabled(true);
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setSupportZoom(true);
		settings.setJavaScriptEnabled(true);

		settings.setAllowContentAccess(true);
		settings.setAllowFileAccess(true);

		final Locale def = Locale.getDefault();
		final String prefix = def.getLanguage().toLowerCase(def);

		final String base = "file:///android_asset/";
		final String baseName = "help_page";
		String dir = prefix + "_" + baseName + "/";
		String file = dir + filename;
		InputStream is;
		try
		{
			is = getAssets().open(file);
			is.close();
		}
		catch (IOException e)
		{
			Log.e(TAG, "Missing localized asset " + file, e);
			dir = baseName + "/";
			file = dir + filename;
		}

		webview.loadUrl(base + file);
	}
}
