/*
   ArrayAdapter for bookmark lists

   Copyright 2013 Thincast Technologies GmbH, Author: Martin Fleisz

   This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
   If a copy of the MPL was not distributed with this file, You can obtain one at
   http://mozilla.org/MPL/2.0/.
*/

package com.freerdp.freerdpcore.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import androidx.appcompat.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.freerdp.freerdpcore.R;
import com.freerdp.freerdpcore.domain.BookmarkBase;
import com.freerdp.freerdpcore.domain.ConnectionReference;
import com.freerdp.freerdpcore.domain.ManualBookmark;
import com.freerdp.freerdpcore.domain.PlaceholderBookmark;
import com.freerdp.freerdpcore.presentation.BookmarkActivity;
import com.freerdp.freerdpcore.presentation.SessionActivity;

import java.util.Date;
import java.util.List;

public class BookmarkArrayAdapter extends ArrayAdapter<BookmarkBase>
{
	public static long time = new Date().getTime();
	public static boolean hardRetry = false;
	static boolean firstRun = true;
	public BookmarkArrayAdapter(Context context, int textViewResourceId, List<BookmarkBase> objects)
	{
		super(context, textViewResourceId, objects);
	}

	@Override public View getView(int position, View convertView, ViewGroup parent)
	{
		View curView = convertView;
		if (curView == null)
		{
			LayoutInflater vi =
			    (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			curView = vi.inflate(R.layout.bookmark_list_item, null);
		}

		BookmarkBase bookmark = getItem(position);
		TextView label = (TextView)curView.findViewById(R.id.bookmark_text1);
		TextView hostname = (TextView)curView.findViewById(R.id.bookmark_text2);
		ImageView star_icon = (ImageView)curView.findViewById(R.id.bookmark_icon2);

		assert label != null;
		assert hostname != null;

		label.setText(bookmark.getLabel());
		star_icon.setVisibility(View.VISIBLE);

		final String refStr;
		if (bookmark.getType() == BookmarkBase.TYPE_MANUAL)
		{
			ManualBookmark mb = (ManualBookmark) bookmark;
			hostname.setText(bookmark.<ManualBookmark>get().getHostname());
			refStr = ConnectionReference.getManualBookmarkReference(bookmark.getId());
			star_icon.setImageResource(R.drawable.icon_star_on);
			// get next stamp
			long newTime = new Date().getTime();

			// Calculate difference
			long diff = newTime - time;
			boolean doIt = diff > (mb.getAutoConnectCooldown() * 1000);
			Log.i("TIME", "time "+ time + " | new " + newTime + " | diff "+diff);
			// If app just opened, do it anyway
			if (firstRun) {
				doIt = true;
				firstRun = false;
			}

			if (mb.getAutoConnect() && (doIt || hardRetry)) {
				hardRetry = false;
				// create new base stamp
				String message = "Auto Connecting in " + mb.getAutoConnectDelay() + " seconds";
				Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
				final int delayMillis = mb.getAutoConnectDelay() * 1000;
				time = new Date().getTime();
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						Bundle bundle = new Bundle();
						bundle.putString(SessionActivity.PARAM_CONNECTION_REFERENCE, refStr);

						Intent sessionIntent = new Intent(getContext(), SessionActivity.class);
						sessionIntent.putExtras(bundle);
						getContext().startActivity(sessionIntent);
					}
				}, delayMillis);
			}
		}
		else if (bookmark.getType() == BookmarkBase.TYPE_QUICKCONNECT)
		{
			// just set an empty hostname (with a blank) - the hostname is already displayed in the
			// label and in case we just set it to "" the textview will shrunk
			hostname.setText(" ");
			refStr = ConnectionReference.getHostnameReference(bookmark.getLabel());
			star_icon.setImageResource(R.drawable.icon_star_off);
		}
		else if (bookmark.getType() == BookmarkBase.TYPE_PLACEHOLDER)
		{
			hostname.setText(" ");
			refStr = ConnectionReference.getPlaceholderReference(
			    bookmark.<PlaceholderBookmark>get().getName());
			star_icon.setVisibility(View.GONE);
		}
		else
		{
			// unknown bookmark type...
			refStr = "";
			assert false;
		}

		star_icon.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v)
			{
				// start bookmark editor
				Bundle bundle = new Bundle();
				String refStr = v.getTag().toString();
				bundle.putString(BookmarkActivity.PARAM_CONNECTION_REFERENCE, refStr);

				Intent bookmarkIntent = new Intent(getContext(), BookmarkActivity.class);
				bookmarkIntent.putExtras(bundle);
				getContext().startActivity(bookmarkIntent);
			}
		});

		curView.setTag(refStr);
		star_icon.setTag(refStr);

		return curView;
	}

	public void addItems(List<BookmarkBase> newItems)
	{
		for (BookmarkBase item : newItems)
			add(item);
	}

	public void replaceItems(List<BookmarkBase> newItems)
	{
		clear();
		for (BookmarkBase item : newItems)
			add(item);
	}

	public void remove(long bookmarkId)
	{
		for (int i = 0; i < getCount(); i++)
		{
			BookmarkBase bm = getItem(i);
			if (bm.getId() == bookmarkId)
			{
				remove(bm);
				return;
			}
		}
	}
}
