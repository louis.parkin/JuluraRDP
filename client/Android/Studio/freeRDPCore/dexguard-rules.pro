# For sane debuggability
-keepattributes SourceFile,LineNumberTable

# Flatten obfuscated files' package structure
# IDE shows the line below as faulty
#-repackageclasses ''

##------------- Begin: proguard configuration for Gson -------------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.entersekt.features.information.faqs.FaqsList { *; }
-keep class com.entersekt.features.information.faqs$* { *; }
##-------------- End: proguard configuration for Gson --------------

##--------- Begin: proguard configuration for SDK ----------
-dontwarn com.google.android.gms.**
##---------- End: proguard configuration for SDK -----------

-dontnote android.net.http.**
-dontnote org.apache.http.**


##--------- Begin: Huawei ----------
-keep class com.huawei.**{*;}
-keep class com.huawei.hms.**{*;}
-keep, includecode class com.huawei.hms.**{*;}
-keep class com.hianalytics.android.**{*;}
-keep class com.huawei.updatesdk.**{*;}
-keepresources string/hms*, string/third_app_*, string/upsdk_*, string/connect_server_fail_prompt_toast, string/getting_message_fail_prompt_toast, string/no_available_network_prompt_toast, string/agc*
-keepresourcefiles agconnect-services.json
##--------- End: Huawei ----------


##------------------------------ Start: Auth App DexGuard ------------------------------

-obfuscatecode,low class com.entersekt.authapp.**
-multidex
-keepresourcefiles res/**

##------------------------------ End: Auth App DexGuard ------------------------------