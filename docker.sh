#!/bin/bash
if [ -z "${NDK_VERSION}" ]; then
  NDK_VERSION="21.3.6528147"
fi

docker run  --rm -it \
            -v "$(pwd)":"/app" \
            -v "/home/louis/Android":"/Android" \
            -e NDK="/Android/Sdk/ndk/${NDK_VERSION}" \
            -e SDK="/Android/Sdk" \
            wolverminion/freerdp/builder:2.0.11 /app/android-in-docker.sh
